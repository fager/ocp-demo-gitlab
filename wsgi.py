from flask import Flask, jsonify, render_template
from dotenv import load_dotenv
import calendar
import socket
import os

load_dotenv('.env')

class Config(object):
  DEBUG = False
  APP_BG_COLOR = os.environ.get("APP_BG_COLOR")

application = Flask(__name__)
application.config.from_object(Config)

@application.route("/")
def index():
    return render_template('index.html')

@application.route("/healthz")
def healthz():
    return "OK"

@application.route("/leapyear/<int:i_year>")
def leapyear( i_year ):
    ret_leap = calendar.isleap(i_year)
    ret_hostname = socket.gethostname()

    return jsonify( { 'isleapyear': ret_leap, 'worker': ret_hostname } )

@application.route("/leapyear.html")
def leapyear_html():
    return render_template('leapyear.html')

if __name__ == "__main__":
    application.run()
