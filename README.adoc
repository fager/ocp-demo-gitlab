= Demoanwendung für OpenShift s2i

== Requirements

Dieses Repository muss in einer Gitumgebung (Gitlab/Github/...) für das
OpenShift-Cluster erreichbar sein, in dem diese Demo durchgeführt werden soll.

Von der Gitumgebung aus, müssen Webhooks auf die OpenShift-API möglich sein.

Info für Entwickler:
- Pipfile ist für Entwickler vorgesehen
- requirements.txt wird aber für OpenShift s2i benötigt.

== Inhalt der Demo

In dieser Demo werden auf der Basis von Branches einfache S2I Abläufe im
OpenShift-Cluster demonstriert.

Es werden zwei OpenShift-Projekte angelegt.
Das eine Projekt simuliert die produktive Anwendung und das zweite Projekt
wird als Entwicklungsumgebung verwendet.

Die beiden Projekte werden im Git durch die Branches "dev" und
"prod" dagestellt.

NOTE: Der main-Branch in diesem Repo wird nicht verändert, um die Demo
mehrfach durchführen zu können.
Die Demo-Commits erfolgen lediglich auf die beiden Projekt-Branches.


== Vorbereitung

Die Demos sollten vor dem Präsentationstermin bereits angelegt werden, weil
die Builds etwas laufen und es ansonsten dauert bis die Anwendung initial
gezeigt werden kann.

.Erstellen der Demos
[source]
----
./k8s_setup.sh
----

