#!/bin/bash


# Generate a Secret in Namespace $1

if [ "${1}" == "" ]
then
  echo "Namespace missing"
  exit 1
fi

NAMESPACE="${1}"

if [ -f ".secret.${NAMESPACE}" ]
then
  SECRET="$(cat ".secret.${NAMESPACE}")"
else
  SECRET="$(uuidgen)"
  printf "%s" "${SECRET}" > ".secret.${NAMESPACE}"
fi


echo "Secret for ${NAMESPACE}: ${SECRET_PROD}"

if ! oc -n "${NAMESPACE}" get secret "gitlab-webhook-token" > /dev/null 2>&1
then
  # Secret existiert noch nicht
  oc -n "${NAMESPACE}" create secret generic "gitlab-webhook-token" --from-literal=WebHookSecretKey=${SECRET}
else
  # Secret existiert bereits
  oc -n "${NAMESPACE}" create secret generic "gitlab-webhook-token" --from-literal=WebHookSecretKey=${SECRET} --dry-run=client -o yaml | oc -n "${NAMESPACE}" replace -f -
fi

