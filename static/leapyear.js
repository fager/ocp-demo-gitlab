
$(document).ready(function () {
  $("#leapyearform").submit(function (event) {
    url= "./leapyear/" + $("#leapyear").val();
    $.ajax({
      type: "GET",
      url: url,
      encode: true,
    }).done(function (data) {
      console.log(data);
      if( data.isleapyear ) {
        $("#leapyearresult").html( "Schaltjahr" ).attr("class","badge badge-primary");
      } else {
        $("#leapyearresult").html( "KEIN Schaltjahr" ).attr("class","badge badge-dark");
      }
      $("#workerinfo").html(data.worker);
    });

    event.preventDefault();
  });
});
