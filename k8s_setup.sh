#!/bin/bash

oc new-project cicd-leapyear-dev
./create-secret.sh cicd-leapyear-dev
oc -n cicd-leapyear-dev apply --wait=true -f ./k8s/
#oc -n cicd-leapyear-dev start-build leapyear

oc new-project cicd-leapyear-prod
./create-secret.sh cicd-leapyear-prod
oc -n cicd-leapyear-prod apply --wait=true -f ./k8s/
#oc -n cicd-leapyear-prod start-build leapyear

